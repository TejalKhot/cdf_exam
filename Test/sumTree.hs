File Edit Options Buffers Tools Haskell Help                                                                                                          
sumC [] k = k 0                                                                                                                                       
sumC (x:xs) k = sumC xs (\acc -> k (x + acc) )                                                                                                        
                                                                                                                                                      
-----------------------------------------------                                                                                                       
                                                                                                                                                      
sumC [1,2,3] id                                                                                                                                       
sumC [2,3] (\acc -> id (1 + acc ))                                                                                                                    
sumC [3] (\acc2 -> (\acc -> id (1 + acc)) (2 + acc2))                                                                                                 
sumC [] (\acc3 -> (\acc2 -> (\acc -> id (1 + acc)) (2 + acc2))) (3 + acc3))                                                                           
(\acc3 -> (\acc2 -> (\acc -> id (1 + acc)) (2 + acc2))) (3 + acc3)) 0                                                                                 
(\acc2 -> (\acc -> id (1 + acc)) (2 + acc2))) (3 + 0)                                                                                                 
(\acc2 -> (\acc -> id (1 + acc)) (2 + acc2))) 3                                                                                                       
(\acc -> id (1 + acc)) (2 + 3)                                                                                                                        
(\acc -> id (1 +  acc)) 5                                                                                                                             
id (1 + 5)                                                                                                                                            
id (6)                                                                                                                                                
6                                                                                                                                                     
                                                                                                                                                      
---------------------------------------------                                                                                                         
                                                                                                                                                      
















-11-:----F1  sumTree.hs     Bot L57   Git-master  (Haskell Ind Doc)-----------------------------------------------------------------------------------

