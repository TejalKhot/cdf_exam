import Data.Char
import System.IO
import System.Directory
import Data.List

main:: IO()
main = do
     putStrLn "Enter path of the file to be uncommented\n"
     flname <-getLine
     contents <- readFile flname
     let flines = lines contents
	 consumeSpace line = foldl (\acc x -> if (not $ isSpace x) then acc++[x] else acc) "" line
	 startsWith line pat = and $ zipWith (\x y -> if (x==y) then True else False) (consumeSpace line) pat
	 endsWith line pat = and $ zipWith (\x y -> if (x==y) then True else False ) (reverse $ consumeSpace line) (reverse $ pat)
	 manageComment acc x
	      | ((last acc == "####MULT LINE COMMENT START") && (endsWith x "-}")) = (take ((length acc)-1) acc) 
	      | ((last acc == "####MULT LINE COMMENT START") && (not (endsWith x "-}"))) = acc
              | ((last acc /= "####MULT LINE COMMENT START") && (not $ endsWith x "-}") && (startsWith x "{-")) = acc ++ ["####MULT LINE COMMENT START"]
	      | ((last acc /= "####MULT LINE COMMENT START") && (endsWith x "-}") && (startsWith x "{-")) = acc
              | ((last acc /= "####MULT LINE COMMENT START") && (startsWith x "--")) = acc
	      | otherwise = acc ++ [x]
         removeComments fileString = foldl (\a y -> a ++ "\n" ++ y) "" (foldl (\acc x -> manageComment acc x) [""] fileString)
     putStrLn (removeComments flines)
